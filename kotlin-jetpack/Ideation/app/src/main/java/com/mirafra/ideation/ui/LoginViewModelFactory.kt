package com.mirafra.ideation.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mirafra.ideation.data.RemoteRepository

class LoginViewModelFactory(private val remoteRepo: RemoteRepository): ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(remoteRepo) as T
    }
}