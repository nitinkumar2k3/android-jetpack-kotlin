package com.mirafra.ideation.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

import com.mirafra.ideation.data.GridIdeaModel

import android.widget.ImageView
import androidx.core.content.ContextCompat.startActivity
import com.mirafra.ideation.LoginActivity
import com.mirafra.ideation.R
import kotlinx.android.synthetic.main.list_grid_view.view.*


class GridBaseAdapter( val context: Context,val allItems: List<GridIdeaModel>) : BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val idea = this.allItems[position]

        var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var ideaView = inflator.inflate(R.layout.list_grid_view, null)
        ideaView.imageView.setImageResource(idea.imageResource!!)
        ideaView.tvContent.text = idea.content!!

        return ideaView
    }

    override fun getItem(position: Int): Any {

            return allItems[position]
        }

        override fun getItemId(position: Int): Long {

            return position.toLong()
        }

        override fun getCount(): Int {

            return allItems.size
        }
    }
