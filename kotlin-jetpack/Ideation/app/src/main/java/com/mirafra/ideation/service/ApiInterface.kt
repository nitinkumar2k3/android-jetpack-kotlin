package com.mirafra.ideation.service

import com.mirafra.ideation.data.*
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*
import java.util.*

/*
A suspending function is simply a function that can be paused and resumed at a later time.
They can execute a long running operation and wait for it to complete without blocking
 */
interface ApiInterface {

    @Headers("Content-Type: application/json")
    @POST("add/")
    suspend fun createNewUser(@Body userModel: UserPostData): Response<CreateUserModel>

    @FormUrlEncoded
    @POST("login/")
    suspend fun login(@Field("email") email: String, @Field("password") first_name: String): Response<LoginDataModel>

   // @Headers("Content-Type: application/json")
    @POST("ideation/")
    suspend fun createTeachIdea(@Body techPostData: TechIdeaPostData): Response<TechIdeaModelData>

    @GET("ideation/?")
    fun getMyTechIdea(@Query("user") email: String):Single<MyTechIdeaModel>

    @GET("ideation/comments?")
    fun getAllComment(@Query("idea_id") idea_id: String) : Single<CommentModel>

    @POST("ideation/comments")
    fun postComment(@Body commentPostDat: CommentPostDat): Single<PostCommentModel>

    @POST("ideation/vote")
    fun postVote(@Body voteData: PostVoteData) : Single<VoteModel>

    @GET("logout?")
        fun logoutUser(@Query("refresh_token") refresh_token: String): Single<LogoutModel>
    @GET("ideation")
    fun getAllTechIdea():Single<MyTechIdeaModel>
}