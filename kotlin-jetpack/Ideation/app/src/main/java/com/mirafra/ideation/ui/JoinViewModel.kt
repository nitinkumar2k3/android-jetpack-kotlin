package com.mirafra.ideation.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mirafra.ideation.data.CreateUserModel
import com.mirafra.ideation.data.RemoteRepository
import com.mirafra.ideation.data.UserPostData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class JoinViewModel(private val remoteRepository: RemoteRepository) : ViewModel() {

    val repository: RemoteRepository = RemoteRepository()

    private val userStatus = MutableLiveData<String>()

    private val createUserData = MutableLiveData<CreateUserModel>()

    fun postCreateUserApi() = createUserData as LiveData<CreateUserModel>


    fun getRemoteUserStatus() {
        var staus = remoteRepository.getRemoteUserStaus()
        //userStatus?.postValue(staus)
        userStatus?.value = staus
    }

    fun getUserStatus() = userStatus as LiveData<String>

    suspend fun callCreateUserApi( userPostData: UserPostData): Response<CreateUserModel> =
        repository.doCreateUserApiCall(userPostData)

    fun postUserData(userPostData: UserPostData){
        CoroutineScope(Dispatchers.IO).launch {
            withContext(Dispatchers.Main){
                val createUserResponse = callCreateUserApi(userPostData)
                if (createUserResponse.isSuccessful){
                    createUserData.postValue(createUserResponse.body())
                    clearData(userPostData)
                }else {
                    // error case
                    when (createUserResponse.code()) {
                        404-> Log.i("Api error", "\"not found")

                        500-> Log.i("Api error", "server broken")

                        else ->  Log.i("Api error", "Error")
                    }
                }
            }
        }

    }

    fun clearData(userPostData: UserPostData){
        userPostData.email = ""
        userPostData.first_name = ""
        userPostData.last_name = ""
    }
}