package com.mirafra.ideation.ui


import android.content.Context
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mirafra.ideation.R
import com.mirafra.ideation.data.RemoteRepository
import com.mirafra.ideation.data.TechIdeaPostData
import com.mirafra.ideation.util.PreferenceHelper
import com.mirafra.ideation.util.Utils
import com.mirafra.ideation.util.toast
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_tech_idea.*
import kotlin.concurrent.thread

/**
 * Use for techIdead fragment
 */
class TechIdeaFragment : Fragment() {

    lateinit var tecIdeaViewModel: TecIdeaViewModel

    companion object {
        fun newInstance() = TechIdeaFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tech_idea, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tecIdeaViewModel =
            ViewModelProviders.of(this, FragmentVMFactory(RemoteRepository.getInstance()))
                .get(TecIdeaViewModel::class.java)
        tecIdeaViewModel.getTechPostDataStatus().observe(this, Observer { result ->
            Utils.removeDimLayout(activity, mainTechContainer)
            result?.let {
                when (result.status) {
                    201 -> {
                        send_anim.visibility = View.VISIBLE
                        send_anim.playAnimation()
                        Handler().postDelayed(Runnable() {
                            send_anim.visibility = View.GONE
                        }, 1500)
                        clearData()

                    }
                    else -> {
                        Toast.makeText(activity, "Error occurred ", Toast.LENGTH_LONG).show()
                    }
                }
            }

            if (result == null){
                Toast.makeText(activity, "Unexpected error", Toast.LENGTH_LONG).show()
            }
        })

        techSubmitBtn.setOnClickListener {
            if (Utils.isInternetAvailable(activity)) {
                if (isValidTitle()) {
                    if (isValidPurpose()) {
                        if (isValidDescription()) {
                            Utils.applyDimLayout(activity, mainTechContainer)
                            tecIdeaViewModel.techPostData(createData(context!!.applicationContext))
                        }
                    }
                }
            } else {
                Utils.createInfoDialog(context, getString(R.string.no_internet))
            }
        }
    }

    override fun onResume() {
        super.onResume()
        send_anim.visibility = View.GONE
    }
    fun createData(context: Context): TechIdeaPostData {
        val prefs = PreferenceHelper.defaultPrefs(context)
        val value: String? = prefs.getString("email", "")
        return TechIdeaPostData(
            value.toString(),
            tvPurpose.text.toString(),
            tvTitle.text.toString(),
            tvDesc.text.toString()
        )
    }

    private fun clearData() {
        tvDesc.text.clear()
        tvTitle.text.clear()
        tvPurpose.text.clear()
    }

    fun isValidTitle(): Boolean {
        var validOrnot = false
        if (tvTitle!!.text.trim().length > 5) {
            validOrnot = true
        } else {
            Toast.makeText(context, "Please enter valid title!", Toast.LENGTH_SHORT).show()
            validOrnot = false
        }
        return validOrnot
    }

    fun isValidPurpose(): Boolean {
        var validOrnot = false
        if (tvPurpose!!.text.trim().length > 5) {
            validOrnot = true
        } else {
            Toast.makeText(context, "Please enter valid purpose!", Toast.LENGTH_SHORT).show()
            validOrnot = false
        }

        return validOrnot
    }

    fun isValidDescription(): Boolean {
        var validOrnot = false
        if (tvDesc!!.text.trim().length > 10) {
            validOrnot = true
        } else {
            Toast.makeText(context, "Please enter valid Description!", Toast.LENGTH_SHORT).show()
            validOrnot = false
        }

        return validOrnot
    }

}
