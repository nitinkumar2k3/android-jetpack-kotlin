package com.mirafra.ideation.data


import com.google.gson.annotations.SerializedName

data class MyTechIdeaModel(
    @SerializedName("data")
    var `data`: List<Data>,
    @SerializedName("msg")
    val msg: String,
    @SerializedName("sessionid")
    val sessionid: Any,
    @SerializedName("status")
    val status: Int
)

{
    data class Data(
        @SerializedName("comments")
        val comments: Int,
        @SerializedName("created")
        val created: String,
        @SerializedName("desc")
        val desc: String,
        @SerializedName("dislikes")
        val dislikes: Int,
        @SerializedName("id")
        val id: Int,
        @SerializedName("idea")
        val idea: String,
        @SerializedName("likes")
        val likes: Int,
        @SerializedName("title")
        val title: String,
        @SerializedName("user")
        val user: String
    )
}