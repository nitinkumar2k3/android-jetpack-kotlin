package com.mirafra.ideation.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mirafra.ideation.data.RemoteRepository

class ForgetPasswordViewModel(private val remoteRepository: RemoteRepository) : ViewModel() {

    private val userPwdStatus = MutableLiveData<String>()

    fun getRemoteUserPwdStatus() {
        var staus = remoteRepository.getRemoteUserPwdStatus()
        userPwdStatus?.postValue(staus)
    }

    fun getUserPwdStatus() = userPwdStatus as LiveData<String>

}