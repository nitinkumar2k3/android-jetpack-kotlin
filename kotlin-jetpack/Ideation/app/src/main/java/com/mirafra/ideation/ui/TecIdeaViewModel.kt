package com.mirafra.ideation.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mirafra.ideation.data.RemoteRepository
import com.mirafra.ideation.data.TechIdeaModelData
import com.mirafra.ideation.data.TechIdeaPostData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.lang.Exception

class TecIdeaViewModel(private val remoteRepository: RemoteRepository) : ViewModel() {

    private val techIdeaStatus = MutableLiveData<TechIdeaModelData>()

    suspend fun sendTechPostData(techIdeaPostData: TechIdeaPostData): Response<TechIdeaModelData> {
        return remoteRepository.createTechIdea(techIdeaPostData)

    }

    fun techPostData(techIdeaPostData: TechIdeaPostData){
        CoroutineScope(Dispatchers.IO).launch {
            withContext(Dispatchers.Main){
                try{
                    val createTechIdea = sendTechPostData(techIdeaPostData)
                    if (createTechIdea.isSuccessful){
                        techIdeaStatus.postValue(createTechIdea.body())
                    }else{
                        Log.i("Api error", ""+createTechIdea.code())
                        techIdeaStatus.postValue(null)
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                    techIdeaStatus.postValue(null)
                }
            }
        }
    }
    fun getTechPostDataStatus() = techIdeaStatus as LiveData<TechIdeaModelData>

}