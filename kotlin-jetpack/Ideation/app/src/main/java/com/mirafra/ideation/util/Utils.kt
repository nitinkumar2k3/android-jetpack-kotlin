package com.mirafra.ideation.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.mirafra.ideation.R
import org.jetbrains.anko.alert

class Utils {

    companion object {
        val BASE_URL = "http://192.168.1.200:81/users/"
        //val BASE_URL = "http://192.168.1.171:8000/users/"
        var accessToken: String? = null
        var refreshToken: String? = null
        var appcontext: Context? = null

        fun isInternetAvailable(context: Context?): Boolean {
            val connectivityManager =
                context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnectedOrConnecting
        }

        fun createInfoDialog(context: Context?, body: String) {
            context?.alert(body) {
                title(R.string.app_name)
                icon(R.mipmap.ic_launcher)
                positiveButton(R.string.dialog_positive_btn) {}
            }?.show()
        }

        fun logoutInfoDialog(context: Context?, body: String) {
            context?.alert(body) {
                title(R.string.app_name)
                icon(R.mipmap.ic_launcher)
                positiveButton(R.string.dialog_positive_btn) {}
                negativeButton(R.string.dialog_negative_btn){}
            }?.show()
        }


        fun openWebview(context: Context, url: String) {
            val uris = Uri.parse(url)
            val intents = Intent(Intent.ACTION_VIEW, uris)
            intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val b = Bundle()
            b.putBoolean("new_window", true)
            intents.putExtras(b)
            context!!.applicationContext.startActivity(intents)
        }

        fun applyDimLayout(activity: Activity?, view: View) {
            activity?.getWindow()?.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            );
            view.setBackgroundColor(activity?.resources!!.getColor(R.color.devider_color))
        }

        fun removeDimLayout(activity: Activity?, view: View) {
            activity?.getWindow()?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            view.setBackgroundColor(activity?.resources!!.getColor(R.color.white))
        }

    }


}