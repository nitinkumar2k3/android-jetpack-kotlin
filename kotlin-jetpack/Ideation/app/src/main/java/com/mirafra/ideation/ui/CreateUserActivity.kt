package com.mirafra.ideation

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mirafra.ideation.data.CreateUserModel
import com.mirafra.ideation.data.RemoteRepository
import com.mirafra.ideation.data.UserPostData
import com.mirafra.ideation.ui.JoinViewModel
import com.mirafra.ideation.ui.JoinViewModelFactory
import com.mirafra.ideation.ui.LoginViewModel
import com.mirafra.ideation.ui.LoginViewModelFactory
import com.mirafra.ideation.util.toast
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.emailEt
import kotlinx.android.synthetic.main.activity_user.*

class CreateUserActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var viewModel : JoinViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        val factory = JoinViewModelFactory(RemoteRepository.getInstance())
        viewModel = ViewModelProviders.of(this, factory).get(JoinViewModel::class.java)


        signUpBtn.setOnClickListener(this)


        viewModel.postCreateUserApi().observe(this, Observer {
            var userResponse: CreateUserModel
            userResponse = it
            Toast.makeText(this,"User created with name "+ userResponse.data.firstName.toString(), Toast.LENGTH_SHORT).show()
            intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
        })

}

    private fun doSignUp(){
        if (isEmailValid()){

            viewModel.postUserData(UserPostData(emailEt.text.toString(), first_nameEt.text.toString(), last_nameEt.text.toString()))
        }
    }

    fun isEmailValid(): Boolean {
        var validOrnot = false;

        if (emailEt!!.text.trim().length > 0 && emailEt!!.text.trim().contains("@mirafra.com")) {
            validOrnot = true
        } else {
            toast(applicationContext, "Please enter valid email")
            validOrnot = false
        }

        return validOrnot;
    }

    fun isPasswordValid(): Boolean {
        var validOrnot = false;

        if (passwordEt!!.text.trim().length > 5) {
            validOrnot = true
        } else {
            this.toast(applicationContext, "Please enter valid Password!")
            validOrnot = false
        }

        return validOrnot
    }

    override fun onClick(v: View?) {

        when (v?.id) {

            R.id.signUpBtn -> doSignUp()
        }
    }
    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}
