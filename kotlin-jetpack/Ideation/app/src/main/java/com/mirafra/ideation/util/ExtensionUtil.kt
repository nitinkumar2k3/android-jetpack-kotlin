package com.mirafra.ideation.util

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.mirafra.ideation.BuildConfig

/**
 *
 * Doc URL:
 *
 * https://www.programiz.com/kotlin-programming/lambdas
 * https://medium.com/@agrawalsuneet/inline-function-kotlin-3f05d2ea1b59
 */
fun AppCompatActivity.addFragment(fragment: Fragment, fragmentId: Int){
     supportFragmentManager.inTransaction { add(fragmentId,fragment) }

}
fun AppCompatActivity.replaceFragment(fragment: Fragment, fragmentId: Int){
    supportFragmentManager.inTransaction { replace(fragmentId,fragment) }
}
// transaction is the name of parameter, function is FragmentTransaction(), return type is FragmentTransaction
inline fun FragmentManager.inTransaction(transaction : FragmentTransaction.() -> FragmentTransaction){
        beginTransaction().transaction().commit()
}

fun String.isEmailValid(name: String): Boolean {
    var validOrnot = false

    validOrnot = name.length > 0 && name.trim().contains("@mirafra.com")

    return validOrnot
}


fun String.isPasswordValid(password: String): Boolean {
    var validOrnot = false;

    validOrnot = password!!.trim().length > 5

    return validOrnot
}

fun Context.toast(context: Context = applicationContext, message: String, duration: Int = Toast.LENGTH_SHORT){
    Toast.makeText(context, message , duration).show()
}
//for taking string from string.xml
fun Context.stringResToast(context: Context = applicationContext, message: Int, toastDuration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(context, context.getString(message), toastDuration).show()
}
fun Context.logd(message: String){
    if (BuildConfig.DEBUG) Log.d(this::class.java.simpleName, message)
}