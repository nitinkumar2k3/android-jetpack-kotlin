package com.mirafra.ideation.service

import com.google.gson.GsonBuilder
import com.mirafra.ideation.util.Utils.Companion.BASE_URL
import com.mirafra.ideation.util.Utils.Companion.accessToken
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import java.io.IOException
import java.net.CookieHandler


class ApiService {
    companion object {
        fun create(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(ApiInterface::class.java)
        }
    }

}

fun getOkHttpClient(): OkHttpClient {
    val interceptor = HttpLoggingInterceptor()
    interceptor.level = HttpLoggingInterceptor.Level.BODY
    val builder = OkHttpClient.Builder()
    builder.addInterceptor(interceptor)
        .connectTimeout(20, TimeUnit.SECONDS)
        .readTimeout(20, TimeUnit.SECONDS)
        .addInterceptor(HeaderInterceptor())
    return builder.build()
}



class HeaderInterceptor : Interceptor {
    private val cookies = HashSet<String>(2)
    var csrftoken: String ?= null
    var sessionid: String ?= null

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        if(accessToken != null){
            val headers = Headers.Builder()
                .add("Content-Type", "application/json")
                .add("Authorization", "Bearer " + accessToken.toString().trim())
                .add("X-csrftoken",  csrftoken.toString().trim())
                .build()


            println("accessToken : "+accessToken)
            request = request?.newBuilder()
                .headers(headers)
                .build()
        }
        println("===================header : "+request.headers.get("Authorization"))
        println("===================header : "+request.headers.get("Content-Type"))
        println("===================header : "+request.headers.get("X-csrftoken"))
        val response = chain.proceed(request)
        if (!response.headers("Set-Cookie").isEmpty()) {
            for (header in response.headers("Set-Cookie")) {
                cookies.add(header)
            }
        }
        for (element in cookies){
            println("Cookies Element: "+ element)
            if(element.contains("csrftoken")){
                csrftoken = element.split("=")[1]
                csrftoken = csrftoken!!.split(";")[0]
                println("csrftoken : "+csrftoken)
            }else if(element.contains("sessionid")){
                sessionid = element.split("=")[1]
                sessionid = sessionid!!.split(";")[0]
                println("sessionid : "+sessionid)
            }
        }
        return response
    }

}

//The Global Objects
//https://medium.com/@elye.project/kotlin-and-retrofit-2-tutorial-with-working-codes-333a4422a890
val retrofitServiceObj by lazy {
    ApiService.create()
}