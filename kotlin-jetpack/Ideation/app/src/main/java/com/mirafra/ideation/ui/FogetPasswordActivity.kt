package com.mirafra.ideation.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mirafra.ideation.R
import com.mirafra.ideation.data.RemoteRepository
import kotlinx.android.synthetic.main.activity_forget_pwd.*

class FogetPasswordActivity : AppCompatActivity() {
    lateinit var loginStat: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_pwd)
        val factory = ForgetPwdModelFactory(RemoteRepository.getInstance())
        val viewModel = ViewModelProviders.of(this, factory).get(ForgetPasswordViewModel::class.java)

        viewModel.getUserPwdStatus().observe(this, Observer { stat ->
            when (stat) {
                "Success" -> loginStat = "sucess"
                else -> loginStat = "error"
            }
        })
        btn_email_reset.setOnClickListener(View.OnClickListener {
            viewModel.getRemoteUserPwdStatus()
            var emailReset = email_recovery.text

            if (emailReset.isEmpty()){
                Toast.makeText(this,"Email Cannot be empty",Toast.LENGTH_LONG)
            }


        })


    }
}