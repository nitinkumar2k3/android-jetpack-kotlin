package com.mirafra.ideation.data

data class UserPostData(var  email: String, var first_name: String,  var last_name: String)