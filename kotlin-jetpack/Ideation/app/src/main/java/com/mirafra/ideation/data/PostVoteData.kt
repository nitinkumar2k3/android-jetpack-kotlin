package com.mirafra.ideation.data

data class PostVoteData(var idea_id: String, var user: String, var like: String)