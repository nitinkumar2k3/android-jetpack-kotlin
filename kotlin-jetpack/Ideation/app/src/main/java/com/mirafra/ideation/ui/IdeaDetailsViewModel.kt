package com.mirafra.ideation.ui

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mirafra.ideation.data.*
import io.reactivex.Observable
import io.reactivex.Single

class IdeaDetailsViewModel (private val remoteRepository: RemoteRepository): ViewModel() {
    private val allComments = MutableLiveData<Single<CommentModel>>()

    fun setComments(ideaId: String) {
        allComments.postValue(remoteRepository.getCommentData(ideaId))
    }

    fun getComments() = allComments as LiveData<Single<CommentModel>>


    private val sendComment = MutableLiveData<Single<PostCommentModel>>()

    fun setPostComments(commentPostDat: CommentPostDat){
        sendComment.postValue(remoteRepository.postCommentData(commentPostDat))
    }

    fun getPostCommentResult() = sendComment as LiveData<Single<PostCommentModel>>


    private val sendLikeDislike = MutableLiveData<Single<VoteModel>>()

    fun setVote(voteData: PostVoteData){
        sendLikeDislike.postValue(remoteRepository.postVoteDate(voteData))
    }

    fun getPostVoteResult() = sendLikeDislike as LiveData<Single<VoteModel>>
}
