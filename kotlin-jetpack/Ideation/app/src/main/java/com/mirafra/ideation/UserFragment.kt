package com.mirafra.ideation


import android.annotation.SuppressLint
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.mirafra.ideation.util.PreferenceHelper
import kotlinx.android.synthetic.main.fragment_user.*

import com.mirafra.ideation.util.PreferenceHelper.get
import com.mirafra.ideation.util.PreferenceHelper.set
import com.mirafra.ideation.util.Utils
import org.jetbrains.anko.internals.AnkoInternals
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class UserFragment : Fragment() {

    var navController : NavController ?= null
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        val prefs = PreferenceHelper.defaultPrefs(context!!.applicationContext)

        try {
            userName_et.text = prefs.get("username", "") +" "+ prefs.get("lastname", "")
            userEmail_et.text = prefs.get("email", "")
            version_et.text = "Version "+BuildConfig.VERSION_NAME
            url_et.setOnClickListener {
                Utils.openWebview(context!!.applicationContext, "https://mirafra.com/")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        /* button.setOnClickListener{
             // without using safe args
             val bundle = bundleOf("UserCount" to 100, "STR_VAL" to "testamount")
             navController!!.navigate(R.id.action_userFragment_to_appInfoFragment,bundle)
         }*/
    }

}
