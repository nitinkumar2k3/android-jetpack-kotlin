package com.mirafra.ideation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.auth0.android.jwt.JWT
import com.mirafra.ideation.data.LogoutViewModel
import com.mirafra.ideation.data.RemoteRepository
import com.mirafra.ideation.ui.FragmentVMFactory
import com.mirafra.ideation.util.PreferenceHelper
import com.mirafra.ideation.util.PreferenceHelper.defaultPrefs
import com.mirafra.ideation.util.PreferenceHelper.get
import com.mirafra.ideation.util.PreferenceHelper.set
import com.mirafra.ideation.util.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert


class MainActivity : AppCompatActivity() {

    lateinit var logoutViewModel: LogoutViewModel
    var compositeDisposable = CompositeDisposable()
    var context : Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val prefs = defaultPrefs(this)
        // setting toolbar
        //toolbar.setBackgroundResource(R.mipmap.ic_launcher)
        //setSupportActionBar(toolbar)
        context = this
        logoutViewModel =
            ViewModelProviders.of(this, FragmentVMFactory(RemoteRepository.getInstance()))
                .get(LogoutViewModel::class.java)

        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        bottom_nav?.setupWithNavController(navController)
        bottom_nav.itemIconTintList = null
        bottom_nav.setOnNavigationItemSelectedListener { item ->
            /* if(item.itemId == R.id.destination_techIdea){
                 root_layout.visibility = View.GONE
             }*/
            when (item.itemId) {
                R.id.destination_techIdea -> {
                    navController.navigate((R.id.techIdeaFragment))
                    return@setOnNavigationItemSelectedListener true

                }
                R.id.destination_businessIdea -> {
                    navController.navigate(R.id.businessIdeaFragment)
                    return@setOnNavigationItemSelectedListener true
                }
                /*   R.id.destination_newCustomer -> {
                       navController.navigate(R.id.newCustomerFragment)
                       return@setOnNavigationItemSelectedListener true
                   }*/
                R.id.destination_userInfo -> {
                    navController.navigate(R.id.userFragment)
                    return@setOnNavigationItemSelectedListener true
                }
            }
            false
        }


        val value: String? = prefs.get("username", "")
        welcome_txt.text = "Welcome:" + value



        try {
            val jwt = JWT(prefs.get("access", "").toString())
            val expiresAt = jwt.expiresAt
            val issuedAt = jwt.issuedAt
            val isExpired = jwt.isExpired(1)

            Log.i("Token", "" + expiresAt + " - " + issuedAt + "-" + isExpired)
        } catch (e: Exception) {
        }


        logout_iv.setOnClickListener {

           // Utils.logoutInfoDialog(this, "Are you sure want to do logout")

            this.alert("Are you sure want to do logout") {
                title(R.string.app_name)
                icon(R.mipmap.ic_launcher)
                positiveButton(R.string.dialog_positive_btn) {
                    try {
                        val jwt = JWT(prefs.get("access", "").toString())
                        val expiresAt = jwt.expiresAt
                        val issuedAt = jwt.issuedAt
                        val isExpired = jwt.isExpired(1)

                        Log.i("Token", "" + expiresAt + " - " + issuedAt + "-" + isExpired)

                        if (isExpired) {
                            // Utils.createInfoDialog(this,"Are you sure want to do logout")
                            Toast.makeText(this@MainActivity, "Session Expired", Toast.LENGTH_SHORT).show()
                            intent = Intent(applicationContext, LoginActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                            startActivity(intent)
                            deletecacheData()
                            finish()
                        } else {
                            //Utils.createInfoDialog(this,"Are you sure want to do logout")
                            logoutViewModel.doLogoutCall(Utils.refreshToken.toString())
                            logoutViewModel.getLogoutStatus().observe(this@MainActivity, Observer {
                                compositeDisposable.add(it
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(
                                        { result ->
                                            intent = Intent(
                                                applicationContext,
                                                SplashScreenActivity::class.java
                                            )
                                            startActivity(intent)
                                            deletecacheData()
                                            finish()
                                        },
                                        { error ->
                                            Toast.makeText(
                                                this@MainActivity,
                                                "Errro while fetch ",
                                                Toast.LENGTH_LONG
                                            ).show()
                                            intent = Intent(
                                                applicationContext,
                                                SplashScreenActivity::class.java
                                            )
                                            startActivity(intent)
                                            deletecacheData()
                                            finish()
                                        }
                                    ))

                            })
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                negativeButton(R.string.dialog_negative_btn) {}
            }?.show()


        }

        /*refreshTokenViewModel.refreshToken(Utils.refreshToken.toString())

        refreshTokenViewModel.getRefreshToken().observe(this, Observer {
            var refreshTokenModel: RefreshTokenModel
            refreshTokenModel = it
            var token = Utils.accessToken
             Log.i("", refreshTokenModel.access)
            token = refreshTokenModel.access
            Utils.accessToken = refreshTokenModel.access

        })*/


    }

    private fun deletecacheData() {
        val prefs = PreferenceHelper.defaultPrefs(this)
        prefs.set("email", "")
        prefs.set("username", "")
        prefs.set("refresh", "")
        prefs.set("access", "")
        prefs.set("isLoggedin", false)
    }


    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
    /*private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
            .replace(R.id.content, fragment, fragment.javaClass.getSimpleName())
            .commit()
    }
    private fun setupBottomNavMenu(navController: NavController) {
        bottom_nav?.let {
            NavigationUI.setupWithNavController(it, navController)
        }
    }*/

}
