package com.mirafra.ideation

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.GridView
import androidx.appcompat.app.AppCompatActivity
import com.mirafra.ideation.data.GridIdeaModel
import com.mirafra.ideation.ui.GridBaseAdapter
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T




class IdeationGridActivity : AppCompatActivity() {

    var ideaList = ArrayList<GridIdeaModel>()
    var adapter : GridBaseAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_ideation)
        val gridview = findViewById(R.id.grid_view) as GridView
        ideaList.add(GridIdeaModel("Tech Idea",R.drawable.tech_idea))
       // ideaList.add(GridIdeaModel("New Idea", R.drawable.my_ideas))
        ideaList.add(GridIdeaModel("Business Idea",R.drawable.all_idea))
       // ideaList.add(GridIdeaModel("User",R.drawable.user_icon))
        adapter = GridBaseAdapter(this,ideaList)
        gridview.adapter = adapter

        gridview.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->
              val selectedItem = parent.getItemAtPosition(position).toString()
            if(position == 0)
              startActivity(Intent(this, LoginActivity::class.java))
          })
    }
}