package com.mirafra.ideation.data


import com.google.gson.annotations.SerializedName

data class PostCommentModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("msg")
    val msg: String,
    @SerializedName("sessionid")
    val sessionid: Any,
    @SerializedName("status")
    val status: Int
) {
    data class Data(
        @SerializedName("comment")
        val comment: String,
        @SerializedName("idea_id")
        val ideaId: String,
        @SerializedName("user")
        val user: String
    )
}