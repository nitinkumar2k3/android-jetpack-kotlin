package com.mirafra.ideation.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mirafra.ideation.MainActivity
import com.mirafra.ideation.R
import com.mirafra.ideation.data.RemoteRepository
import kotlinx.android.synthetic.main.activity_join_ideation.*

class JoinActivity : AppCompatActivity() {

    lateinit var loginStat: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_ideation)
        val factory = JoinViewModelFactory(RemoteRepository.getInstance())
        val viewModel = ViewModelProviders.of(this, factory).get(JoinViewModel::class.java)

        viewModel.getUserStatus().observe(this, Observer { stat ->
            when (stat) {
                "sucess" -> startActivity(Intent(this, MainActivity::class.java))
                "error" -> {
                    Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
                }
                else ->
                    Toast.makeText(this, "Unexpeced error", Toast.LENGTH_LONG).show()
            }
        })

        signUp.setOnClickListener {
            viewModel.getRemoteUserStatus()
        }

    }

}
