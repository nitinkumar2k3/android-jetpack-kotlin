package com.mirafra.ideation.data

import android.os.Parcel
import android.os.Parcelable


//data class GridIdeaModel(var  content: String, var ImageResource: String)
data class GridIdeaModel(var content: String?, var imageResource: Int?)
