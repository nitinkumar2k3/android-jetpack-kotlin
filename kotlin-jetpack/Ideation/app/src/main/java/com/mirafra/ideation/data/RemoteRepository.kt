package com.mirafra.ideation.data

import android.app.Activity
import android.content.Context
import com.mirafra.ideation.service.retrofitServiceObj
import com.mirafra.ideation.service.ApiInterface
import com.mirafra.ideation.util.PreferenceHelper
import io.reactivex.Single
import retrofit2.Response

class RemoteRepository {
    var client: ApiInterface = retrofitServiceObj
    companion object{
        @Volatile private var  instance: RemoteRepository ?= null

        fun getInstance()=
            instance?: synchronized(this){
                instance ?: RemoteRepository().also { instance = it }
            }
    }

    fun getRemoteUserStaus(): String{
        //todo retrofit call
        return "sucess"
    }

    fun getRemoteUserPwdStatus(): String{
        return "Success"
    }

    suspend fun createTechIdea(techIdeaPostData: TechIdeaPostData): Response<TechIdeaModelData> {
        return client.createTeachIdea(techIdeaPostData)
    }
    suspend fun doCreateUserApiCall(userdata : UserPostData) = client.createNewUser(userdata)

    suspend fun doLogin(email: String, password: String) = client.login(email, password)


    fun getMyTechIdea(context: Context): Single<MyTechIdeaModel>{
        val prefs = PreferenceHelper.defaultPrefs(context)
        val value: String? = prefs.getString("email","")
        return client.getMyTechIdea(value.toString())
    }
    fun getMyAllTechIdea(context: Context): Single<MyTechIdeaModel>{
        return client.getAllTechIdea()
    }

    fun getCommentData(ideaId: String) : Single<CommentModel>{
        return client.getAllComment(ideaId)
    }

    fun postCommentData(commentPostDat: CommentPostDat) : Single<PostCommentModel>{
        return client.postComment(commentPostDat)
    }

    fun postVoteDate(voteData: PostVoteData) : Single<VoteModel>{
        return client.postVote(voteData)
    }

    fun logoutCurrentUser(refreshToken: String): Single<LogoutModel>{
        return client.logoutUser(refreshToken)
    }
}