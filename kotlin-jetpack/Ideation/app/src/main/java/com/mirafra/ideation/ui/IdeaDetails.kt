package com.mirafra.ideation.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.mirafra.ideation.data.CommentPostDat
import com.mirafra.ideation.data.PostVoteData
import com.mirafra.ideation.data.RemoteRepository
import com.mirafra.ideation.util.CommentAdapter
import com.mirafra.ideation.util.PreferenceHelper
import com.mirafra.ideation.util.PreferenceHelper.get
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.idea_details_fragment.*


class IdeaDetails() : Fragment() {

    var compositeDisposable = CompositeDisposable()
    private lateinit var viewModel: IdeaDetailsViewModel
    lateinit var idea: String
    lateinit var title: String
    lateinit var desc: String
    lateinit var like: String
    lateinit var dislike: String
    lateinit var comment: String
    lateinit var idea_id: String
    lateinit var email: String
    lateinit var USER: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            com.mirafra.ideation.R.layout.idea_details_fragment,
            container,
            false
        )
    }

    @SuppressLint("NewApi", "SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val prefs = PreferenceHelper.defaultPrefs(activity!!.applicationContext)

        email = prefs.get("email", "")!!

        idea = arguments!!.getString("IDEA").toString()
        title = arguments!!.getString("TITLE").toString()
        desc = arguments!!.getString("DESC").toString()
        like = arguments!!.getString("LIKE").toString()
        dislike = arguments!!.getString("DISLIKE").toString()
        comment = arguments!!.getString("COMMENT").toString()
        idea_id = arguments!!.getString("IDEA_ID").toString()
        USER = arguments!!.getString("USER").toString()

        idea_titleTV.text = "Title : " + title
        idea_purposeTV.text = "Purpose : " + idea
        idea_description.text = "Description : " + desc
        idea_user.text = "User : " + email

        like_tv.text = like + ""
        dislike_tv.text = dislike + ""
        comment_tv.text = comment + ""


        comment_recycler.layoutManager = LinearLayoutManager(activity)


        viewModel =
            ViewModelProviders.of(this, FragmentVMFactory(RemoteRepository.getInstance()))
                .get(IdeaDetailsViewModel::class.java)
        viewModel.setComments(idea_id)
        viewModel.getComments().observe(this, Observer {
            compositeDisposable.add(it
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->

                        comment_recycler?.let {
                            comment_recycler.adapter =
                                CommentAdapter(result, activity!!.applicationContext)
                            comment_recycler.scrollToPosition(result.data.size - 1);
                        }

                    },
                    { error ->
                        activity?.let {
                            Toast.makeText(
                                activity,
                                "Errro while fetch ",
                                Toast.LENGTH_LONG
                            ).show()
                        }

                    }
                ))

        })

        send_comment.setOnClickListener {
            if (comment_et.text.trim().length > 0) {
                var commentPostDat = CommentPostDat(email, idea_id, comment_et.text.toString())
                viewModel.setPostComments(commentPostDat)
            } else {
                Toast.makeText(activity, "Please type comment!", Toast.LENGTH_LONG).show()
            }

        }

        like_ll.setOnClickListener {
            var voteData = PostVoteData(idea_id, email, "0")
            viewModel.setVote(voteData)
        }

        dislike_ll.setOnClickListener {
            var voteData = PostVoteData(idea_id, email, "1")
            viewModel.setVote(voteData)
        }

        viewModel.getPostVoteResult().observe(this, Observer {
            compositeDisposable.add(it
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        if (result.status == 202) {

                            if (result.data.like == true) {
                                like_tv.text = (like.toInt() + 1).toString() + ""
                                dislike_tv.text = (dislike.toInt() - 1).toString() + ""
                                like = like_tv.text.toString()
                                dislike = dislike_tv.text.toString()

                                Toast.makeText(activity, "Like done", Toast.LENGTH_LONG).show()
                            } else {
                                dislike_tv.text = (dislike.toInt() + 1).toString() + ""
                                like_tv.text = (like.toInt() - 1).toString() + ""

                                like = like_tv.text.toString()
                                dislike = dislike_tv.text.toString()

                                Toast.makeText(activity, "Un-like done", Toast.LENGTH_LONG)
                                    .show()
                            }
                        }
                    },
                    { error ->
                        Toast.makeText(activity, "Errro while fetch ", Toast.LENGTH_LONG).show()
                    }
                ))
        })

        viewModel.getPostCommentResult().observe(this, Observer {
            compositeDisposable.add(it
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        if (result.status == 201) {
                            Toast.makeText(activity, result.msg.toString(), Toast.LENGTH_LONG)
                                .show()
                            comment_et.setText("")
                            viewModel.setComments(idea_id)
                            comment_tv.text = (comment.toInt() + 1).toString() + ""
                            comment = comment_tv.text.toString()
                        }
                    },
                    { error ->
                        Toast.makeText(activity, "Errro while fetch ", Toast.LENGTH_LONG).show()
                    }
                ))
        })

        /*comment_et.setOnClickListener {
            comment_et.isFocusable = true
            comment_et.isFocusableInTouchMode = true
        }*/

        comment_et.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                if (s.length > 0) {
                    send_comment.setImageResource(com.mirafra.ideation.R.drawable.send_comment)
                } else {
                    send_comment.setImageResource(com.mirafra.ideation.R.drawable.send_comment_inactive)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(IdeaDetailsViewModel::class.java)

    }


}
