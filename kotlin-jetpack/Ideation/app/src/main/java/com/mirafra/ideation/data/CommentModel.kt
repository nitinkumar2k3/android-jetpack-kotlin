package com.mirafra.ideation.data


import com.google.gson.annotations.SerializedName

data class CommentModel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("msg")
    val msg: String,
    @SerializedName("sessionid")
    val sessionid: Any,
    @SerializedName("status")
    val status: Int
) {
    data class Data(
        @SerializedName("comment")
        val comment: String,
        @SerializedName("created")
        val created: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("idea_id")
        val ideaId: String,
        @SerializedName("modified")
        val modified: String,
        @SerializedName("user")
        val user: String
    )
}