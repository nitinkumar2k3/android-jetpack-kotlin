package com.mirafra.ideation

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mirafra.ideation.data.RemoteRepository
import com.mirafra.ideation.ui.LoginViewModel
import com.mirafra.ideation.ui.LoginViewModelFactory
import com.mirafra.ideation.util.PreferenceHelper
import com.mirafra.ideation.util.PreferenceHelper.set
import com.mirafra.ideation.util.Utils
import com.mirafra.ideation.util.toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val factory = LoginViewModelFactory(RemoteRepository.getInstance())
        viewModel = ViewModelProviders.of(this, factory).get(LoginViewModel::class.java)

        passwordEt.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        passwordEt.setSelection(passwordEt.text.length)

        signupBtn.setOnClickListener(this)

        loginUserBtn.setOnClickListener(this)


        viewModel.postLoginApi().observe(this, Observer {loginResponse ->
            loginResponse?.let {

                if (loginResponse.status == 200) {
                    loginResponse?.let {
                        val prefs = PreferenceHelper.defaultPrefs(this)
                        prefs.set("email", loginResponse.data.email)
                        prefs.set("username", loginResponse.data.firstName)
                        prefs.set("lastname", loginResponse.data.lastName)

                        prefs.set("refresh", loginResponse.refresh)
                        prefs.set("access", loginResponse.access)

                        prefs["isLoggedin"] = true
                        navigateToDashboard()
                    }

                } else {
                    Toast.makeText(this, loginResponse.msg, Toast.LENGTH_LONG).show()
                }
            }

            if (loginResponse == null){
                Toast.makeText(this, "Unexpected error", Toast.LENGTH_LONG).show()
            }
            Utils.removeDimLayout(this, mainContainer)
        })
    }

    private fun navigateToCreateuserScreen() {
        intent = Intent(applicationContext, CreateUserActivity::class.java)
        startActivity(intent)
    }


    private fun navigateToDashboard() {
    //    Toast.makeText(this, "Login Sucess!", Toast.LENGTH_SHORT).show()
        intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
    }

    private fun doUserLoginCall() {
        //if (isEmailValid()) {
        //    if (isPasswordValid()) {
        Utils.applyDimLayout(this,mainContainer)
        viewModel.postLoginData(emailEt.text.toString(), passwordEt.text.toString())
        //     }
        //  }
    }


    private fun isEmailValid(): Boolean {
        var validOrnot = false;

        if (emailEt!!.text.trim().length > 0 && emailEt!!.text.trim().contains("@mirafra.com")) {
            validOrnot = true
        } else {
            toast(applicationContext, "Please enter valid email")
            validOrnot = false
        }

        return validOrnot;
    }

    private fun isPasswordValid(): Boolean {
        var validOrnot = false;

        if (passwordEt!!.text.trim().length > 5) {
            validOrnot = true
        } else {
            this.toast(applicationContext, "Please enter valid Password!")
            validOrnot = false
        }

        return validOrnot
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.loginUserBtn -> {
                if (Utils.isInternetAvailable(this))
                    doUserLoginCall()
                else
                    Utils.createInfoDialog(this, getString(R.string.no_internet))
            }

            R.id.signupBtn -> navigateToCreateuserScreen()
        }
    }

}