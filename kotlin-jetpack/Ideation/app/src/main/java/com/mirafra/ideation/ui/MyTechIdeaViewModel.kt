package com.mirafra.ideation.ui


import android.app.Activity
import android.content.Context
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mirafra.ideation.data.MyTechIdeaModel
import com.mirafra.ideation.data.RemoteRepository
import com.mirafra.ideation.service.ApiInterface
import io.reactivex.Single
class MyTechIdeaViewModel(private val remoteRepository: RemoteRepository) : ViewModel() {

    private val myTechIdeaStatus = MutableLiveData<Single<MyTechIdeaModel>>()

    fun setMyIdea(context: Context) {
        myTechIdeaStatus.postValue(remoteRepository.getMyTechIdea(context))
    }

    fun getMyIdea() = myTechIdeaStatus as LiveData<Single<MyTechIdeaModel>>

    fun getAllIdea() = myTechIdeaStatus as LiveData<Single<MyTechIdeaModel>>

    fun setAllIdea(context: Context) {
        myTechIdeaStatus.postValue(remoteRepository.getMyAllTechIdea(context))
    }

}