package com.mirafra.ideation.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mirafra.ideation.data.RemoteRepository

class JoinViewModelFactory(private val remoteRepo: RemoteRepository): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

       // if( RemoteRepository.javaClass is  Class<*>)
        return JoinViewModel(remoteRepo) as T
    }
}