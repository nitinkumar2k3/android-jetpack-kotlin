package com.mirafra.ideation.data


import com.google.gson.annotations.SerializedName

data class LoginDataModel(
    @SerializedName("access")
    val access: String,
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("msg")
    val msg: String,
    @SerializedName("refresh")
    val refresh: String,
    @SerializedName("sessionid")
    val sessionid: String,
    @SerializedName("status")
    val status: Int
) {
    data class Data(
        @SerializedName("email")
        val email: String,
        @SerializedName("emp_desig")
        val empDesig: String,
        @SerializedName("emp_id")
        val empId: String,
        @SerializedName("first_name")
        val firstName: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("last_name")
        val lastName: String,
        @SerializedName("phone")
        val phone: String,
        @SerializedName("profile_picture")
        val profilePicture: String,
        @SerializedName("scopes")
        val scopes: List<String>,
        @SerializedName("user_group")
        val userGroup: String
    )
}