package com.mirafra.ideation.data


import com.google.gson.annotations.SerializedName

data class TechIdeaModelData(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("msg")
    val msg: String,
    @SerializedName("sessionid")
    val sessionid: String,
    @SerializedName("status")
    val status: Int
) {
    data class Data(
        @SerializedName("desc")
        val desc: String,
        @SerializedName("idea")
        val idea: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("user")
        val user: String
    )
}