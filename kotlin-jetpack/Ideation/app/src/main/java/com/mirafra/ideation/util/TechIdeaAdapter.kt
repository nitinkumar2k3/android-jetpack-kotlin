package com.mirafra.ideation.util

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView

import com.mirafra.ideation.data.MyTechIdeaModel
import com.mirafra.ideation.ui.MyTechIdeaFragment
import com.mirafra.ideation.ui.TechIdeaFragment
import kotlinx.android.synthetic.main.idea_item.view.*
import java.nio.file.DirectoryStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Filter
import kotlin.collections.ArrayList
import androidx.navigation.findNavController
import com.mirafra.ideation.R

import com.mirafra.ideation.ui.MyTechIdeaFragmentDirections


class TechIdeaAdapter(var myTechIdeaModel: List<MyTechIdeaModel.Data>, val context: Context) :
    RecyclerView.Adapter<TechIdeaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.idea_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        try {
            var dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

            var date = dateFormat.parse(myTechIdeaModel.get(position).created.toString().split(".")[0])
            Log.i("__Date", date.toString())

            var outputDateFormat = SimpleDateFormat("dd-MMM-yyyy hh:mm a", Locale.getDefault())
            var outputdate = outputDateFormat.format(date)
            Log.i("__Date 1", outputdate)
            holder.tvTime.text = outputdate.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //  list.add(myTechIdeaModel.data)
        holder.tvTitle.text = "Title : " + myTechIdeaModel.get(position).title
        holder.tvPurpose.text = "Purpose : " + myTechIdeaModel.get(position).idea
        holder.tvdesc.text = "Description : " + myTechIdeaModel.get(position).desc
        holder.tvUser.text = "User : " + myTechIdeaModel.get(position).user
        holder.tvlike.text = myTechIdeaModel.get(position).likes.toString()
        holder.tvdislike.text = myTechIdeaModel.get(position).dislikes.toString()
        holder.tvcomment.text = myTechIdeaModel.get(position).comments.toString()

        holder.itemCard.setOnClickListener {
            try {
                var bundle = bundleOf("IDEA" to myTechIdeaModel.get(position).idea,
                   "TITLE" to myTechIdeaModel.get(position).title,
                    "DESC" to myTechIdeaModel.get(position).desc,
                    "LIKE" to myTechIdeaModel.get(position).likes.toString(),
                    "DISLIKE" to myTechIdeaModel.get(position).dislikes.toString(),
                    "COMMENT" to myTechIdeaModel.get(position).comments.toString(),
                    "IDEA_ID" to myTechIdeaModel.get(position).id.toString(),
                    "USER" to myTechIdeaModel.get(position).user.toString())

                holder.itemCard.findNavController().navigate(R.id.action_businessIdeaFragment_to_ideaDetailsFrag, bundle)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    override fun getItemCount(): Int {
        return myTechIdeaModel.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle = view.idea_title
        val tvPurpose = view.idea_purpose
        val tvdesc = view.idea_description
        val tvUser = view.idea_user
        val tvlike = view.like_tv
        val tvdislike = view.dislike_tv
        val tvcomment = view.comment_tv
        val likeIv = view.like_ll
        val dislikeIv = view.dislike_ll
        val comment = view.comment_ll
        val tvTime = view.idea_time
        val itemCard = view.tech_idea_card


    }
    // To get the data to search Category
    fun filterList(filteredCourseList: List<MyTechIdeaModel.Data>) {
        this.myTechIdeaModel = filteredCourseList
        notifyDataSetChanged()
    }

}