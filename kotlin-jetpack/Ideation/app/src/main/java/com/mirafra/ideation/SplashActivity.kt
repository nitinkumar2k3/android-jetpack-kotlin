package com.mirafra.ideation

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.animation.AlphaAnimation
import androidx.appcompat.app.AppCompatActivity
import com.mirafra.ideation.util.PreferenceHelper
import com.mirafra.ideation.util.PreferenceHelper.get
import kotlinx.android.synthetic.main.activity_splash.*


class SplashScreenActivity : AppCompatActivity() {
    private val SPLASH_TIME_OUT: Long = 1000 // 3 sec


    var validUser = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val prefs = PreferenceHelper.defaultPrefs(this)
        val isloggedIn: Boolean? = prefs["isLoggedin"]


        val animation1 = AlphaAnimation(0.4f, 2.0f)
        animation1.duration = 3000
        animation1.startOffset = 3000
        animation1.fillAfter = true
        text_app_name.startAnimation(animation1)
        Handler().postDelayed({
            if (validUser)
                startActivity(Intent(this, MainActivity::class.java))
            else {
                if (isloggedIn!!) {
                    startActivity(Intent(this, MainActivity::class.java))
                } else {
                    startActivity(Intent(this, IdeationGridActivity::class.java))
                }

                finish()
            }
        }, SPLASH_TIME_OUT)
    }
}