package com.mirafra.ideation.data

data class TechIdeaPostData(var user: String, val idea: String, val title: String, val desc: String)