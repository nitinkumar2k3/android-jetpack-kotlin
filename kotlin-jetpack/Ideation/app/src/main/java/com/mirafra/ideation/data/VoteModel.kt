package com.mirafra.ideation.data


import com.google.gson.annotations.SerializedName

data class VoteModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("msg")
    val msg: String,
    @SerializedName("sessionid")
    val sessionid: Any,
    @SerializedName("status")
    val status: Int
) {
    data class Data(
        @SerializedName("idea_id")
        val ideaId: String,
        @SerializedName("like")
        val like: Boolean,
        @SerializedName("user")
        val user: String
    )
}