package com.mirafra.ideation.data

import com.google.gson.annotations.SerializedName

data class SignUpModel(
    @SerializedName("email")
    val email: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("last_name")
    val lastName: String
)