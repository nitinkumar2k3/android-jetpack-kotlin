package com.mirafra.ideation.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Single

class LogoutViewModel(private val remoteRepository: RemoteRepository) : ViewModel() {
    private val logoutStatus = MutableLiveData<Single<LogoutModel>>()

    fun doLogoutCall(refreshToken: String) {
        logoutStatus.postValue(remoteRepository.logoutCurrentUser(refreshToken))
    }

    fun getLogoutStatus() = logoutStatus as LiveData<Single<LogoutModel>>
}