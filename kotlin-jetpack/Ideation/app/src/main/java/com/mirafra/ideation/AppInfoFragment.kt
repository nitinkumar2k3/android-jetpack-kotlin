package com.mirafra.ideation


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.fragment_app_info.*

/**
 * A simple [Fragment] subclass.
 */
class AppInfoFragment : Fragment() {

    var userCount : Int ?= null
    lateinit var stringNAme : String
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_app_info, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // geting bundle data without safe args
         userCount = arguments!!.getInt("UserCount")
         stringNAme = arguments!!.getString("STR_VAL","defa")


    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userText!!.text = userCount.toString() + " "+ stringNAme

    }

}
