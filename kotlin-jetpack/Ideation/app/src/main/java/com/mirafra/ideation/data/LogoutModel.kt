package com.mirafra.ideation.data


import com.google.gson.annotations.SerializedName

data class LogoutModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("msg")
    val msg: String,
    @SerializedName("sessionid")
    val sessionid: Any,
    @SerializedName("status")
    val status: Int
) {
    class Data(
    )
}