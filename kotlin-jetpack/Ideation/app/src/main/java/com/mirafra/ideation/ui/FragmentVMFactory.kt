package com.mirafra.ideation.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mirafra.ideation.data.LogoutViewModel
import com.mirafra.ideation.data.RemoteRepository


class FragmentVMFactory(private val remoteRepo: RemoteRepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TecIdeaViewModel::class.java)) {
            return TecIdeaViewModel(remoteRepo) as T
        } else if (modelClass.isAssignableFrom(MyTechIdeaViewModel::class.java)) {
            return MyTechIdeaViewModel(remoteRepo) as T
        } else if (modelClass.isAssignableFrom(LogoutViewModel::class.java)) {
            return LogoutViewModel(remoteRepo) as T
        }  else if (modelClass.isAssignableFrom(IdeaDetailsViewModel::class.java)) {
            return IdeaDetailsViewModel(remoteRepo) as T
        }
        throw IllegalArgumentException("Unsupported Model class")
    }
}