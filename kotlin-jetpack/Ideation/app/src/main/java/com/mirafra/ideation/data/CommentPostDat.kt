package com.mirafra.ideation.data

data class CommentPostDat(var user: String, var idea_id: String, var comment: String)