package com.mirafra.ideation.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.mirafra.ideation.R
import com.mirafra.ideation.data.MyTechIdeaModel
import com.mirafra.ideation.data.RemoteRepository
import com.mirafra.ideation.util.TechIdeaAdapter
import com.mirafra.ideation.util.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_my_techidea.*
import kotlinx.android.synthetic.main.idea_item.*
import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.error_connection_internet.*


/**
 * A simple [Fragment] subclass.
 */
class MyTechIdeaFragment : Fragment() {

    lateinit var myTechIdeaViewModel: MyTechIdeaViewModel
    //var list = ArrayList<MyTechIdeaModel>()
    var compositeDisposable = CompositeDisposable()
    lateinit var ideaList: List<MyTechIdeaModel.Data>
    // var customerFragment = NewCustomerFragment()
    lateinit var adapter: TechIdeaAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.mirafra.ideation.R.layout.fragment_my_techidea, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val searchView = view.findViewById<SearchView>(com.mirafra.ideation.R.id.sample_editText)
        val id = searchView.getContext()
            .getResources()
            .getIdentifier("android:id/search_src_text", null, null)
        val textView = searchView.findViewById<TextView>(id)
        textView.setTextColor(Color.BLACK)
        textView.setHint("Search")
        textView.setHintTextColor(Color.GRAY)

        sample_editText.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                filter(p0.toString())
                return true

            }

        })
        myTechIdeaViewModel =
            ViewModelProviders.of(this, FragmentVMFactory(RemoteRepository.getInstance()))
                .get(MyTechIdeaViewModel::class.java)

        if (Utils.isInternetAvailable(context)) {
            myTechIdeaViewModel.setMyIdea(activity!!.applicationContext)
           // idea_title_txt.text = "My Idea"
        } else {
            Utils.createInfoDialog(context, getString(com.mirafra.ideation.R.string.no_internet))
        }
        idea_title_txt.text = "My Idea"
        switch_idea.setOnCheckedChangeListener { b, isChecked ->

            if (Utils.isInternetAvailable(context)) {
                if (isChecked) {
                    idea_title_txt.text = "All Idea"
                    myTechContainer.visibility = View.VISIBLE
                    myTechIdeaViewModel.setAllIdea(activity!!.applicationContext)
                    //searchView.setQuery("",true)
                    searchView.setQuery("",false)
                } else {
                    idea_title_txt.text = "My Idea"
                    myTechContainer.visibility = View.VISIBLE
                    myTechIdeaViewModel.setMyIdea(activity!!.applicationContext)
                    searchView.setQuery("",false)
                }
            } else {
                Utils.createInfoDialog(context, getString(com.mirafra.ideation.R.string.no_internet))
            }
        }

        idea_recyclerview.layoutManager = LinearLayoutManager(activity)

        myTechIdeaViewModel.getMyIdea().observe(this, Observer {

            compositeDisposable.add(it
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { res ->
                        ideaList = res.data
                        adapter = TechIdeaAdapter(ideaList, activity!!.applicationContext)
                        idea_recyclerview?.let {
                            idea_recyclerview.adapter = adapter
                        }

                    },
                    { error ->
                        error_connecttt.visibility = View.VISIBLE

                    }
                ))
            setTitle()
        })
    }

    // myTechIdeaViewModel.setAllIdea(activity!!.applicationContext)
    /*  idea_recyclerview.layoutManager = LinearLayoutManager(activity)

          myTechIdeaViewModel.getMyIdea().observe(this, Observer {

              compositeDisposable.add(it
                  .subscribeOn(Schedulers.io())
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe(
                      { result ->
                          idea_recyclerview?.let {
                              idea_recyclerview?.adapter = TechIdeaAdapter(result, activity!!.applicationContext)
                          }
                      },
                      { error ->
                          activity?.let{Toast.makeText(activity, "Errro while fetch ", Toast.LENGTH_LONG).show()}

                      }
                  ))
              setTitle()
          })
      }*/

    fun setTitle() {
        myTechContainer.visibility = View.GONE
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        myTechIdeaViewModel =
            ViewModelProviders.of(this, FragmentVMFactory(RemoteRepository.getInstance()))
                .get(MyTechIdeaViewModel::class.java)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }


    fun filter(text: String) {

        try {
            val filteredTechIdea: ArrayList<MyTechIdeaModel.Data> = ArrayList()

            val totalIdeas: List<MyTechIdeaModel.Data> = ideaList

            for (eachIdea in totalIdeas) {
                if (eachIdea.idea!!.toLowerCase().contains(text.toLowerCase())) {
                    filteredTechIdea!!.add(eachIdea)
                }
            }
            adapter.filterList(filteredTechIdea!!.toList())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //calling a method of the adapter class and passing the filtered list

    }
}
