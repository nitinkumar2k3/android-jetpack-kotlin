package com.mirafra.ideation.ui

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mirafra.ideation.data.CreateUserModel
import com.mirafra.ideation.data.LoginDataModel
import com.mirafra.ideation.data.RemoteRepository
import com.mirafra.ideation.data.UserPostData
import com.mirafra.ideation.util.Utils.Companion.accessToken
import com.mirafra.ideation.util.Utils.Companion.refreshToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class LoginViewModel(private val remoteRepository: RemoteRepository) : ViewModel() {

    val repository: RemoteRepository = RemoteRepository()

    //private val loginStatus = MutableLiveData<String>()
    private val loginData = MutableLiveData<LoginDataModel>()

    fun postLoginApi() = loginData as LiveData<LoginDataModel>

    suspend fun callLoginApi(email: String, password: String): Response<LoginDataModel> =
        repository.doLogin(email, password)



    fun postLoginData(email: String, password: String) {
        CoroutineScope(Dispatchers.IO).launch {
            withContext(Dispatchers.Main) {
                try {
                    val loginResponse = callLoginApi(email, password)
                    if (loginResponse.isSuccessful) {
                        loginData.postValue(loginResponse.body())
                        accessToken = loginResponse.body()!!.access;
                        refreshToken = loginResponse.body()!!.refresh;

                    } else {
                        loginData.postValue(null)
                        // error case for api response
                        when (loginResponse.code()) {
                            404 -> Log.i("Api error", "\"not found")

                            500 -> Log.i("Api error", "server broken")

                            else -> Log.i("Api error", "Error")
                        }
                    }
                } catch (se: Exception) {
                    loginData.postValue(null)
                    Log.e("LOGIN postLoginData", "Error: ${se.message}")
                }
            }
        }
    }


}