package com.mirafra.ideation.util

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mirafra.ideation.R
import com.mirafra.ideation.data.CommentModel
import kotlinx.android.synthetic.main.comment_item.view.*
import kotlinx.android.synthetic.main.idea_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class CommentAdapter(val commentModel: CommentModel, val context: Context) : RecyclerView.Adapter<CommentAdapter.ViewHolder>() {
    override fun getItemCount(): Int {
        return commentModel.data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.commentTv.text = commentModel.data.get(position).comment

        var dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        var date = dateFormat.parse(commentModel.data.get(position).created)
        var outputDateFormat = SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault())
        var outputdate = outputDateFormat.format(date)
        holder.commentTimeTv.text = outputdate
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.comment_item,
                parent,
                false
            )
        )
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val commentTv = view.comment_Tv
        val commentTimeTv = view.comment_time_tv
    }

}