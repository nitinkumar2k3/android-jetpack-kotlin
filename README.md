# README #

Android Kotlin Application using MVVM architecture.
Using Jetpack Libraries like Navigation, Live data, Mutable Livedata , Androidx, DataBinding, ViewModels
Use of RxKotlin, Coroutines, Retrofit.

### What is this repository for? ###

Its a sample feedback app from the user.They can see all the other users feedback also.
All API call are RESTAPI call.
User Creation and validation flow, Forget passowrd flow,Logout flow and Adding,
updating,like and unlike of the Feedback are the main components of the sample app. 